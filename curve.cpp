#include "curve.h"
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <iterator>

Curve::Curve()
{
}

Curve::Curve(int k, bool bezierCurve)
{
    this->k = k;
    this->bezierCurve = bezierCurve;
}

void Curve::AddPoint(Vector2f point)
{
    points.push_back(point);
    GenerateKnots();
}

void Curve::AddPoint(float x, float y)
{
    AddPoint({x, y});
}

void Curve::ChangePoint(int index, Vector2f point)
{
    points[index] = point;
}

void Curve::InsertPoint(int index, Vector2f point)
{
    std::vector<Vector2f>::iterator iterator = points.begin();
    points.insert(iterator + index + 1, point);
    GenerateKnots();
}

void Curve::RemovePoint(int index)
{
    std::vector<Vector2f>::iterator iterator = points.begin();
    points.erase(iterator + index);
    GenerateKnots();
}

std::tuple<int, float> Curve::ClosestPoint(Vector2f clickPoint)
{
    int minIndex;
    float min = 999999;
    for(int i = 0; i < points.size(); i++)
    {
        float dist = (clickPoint - points[i]).norm();
        if(dist < min)
        {
            min = dist;
            minIndex = i;
        }
    }
    return std::make_tuple(minIndex, min);
}

Vector2f Curve::DeCasteljau(float t)
{
    Vector2f b[points.size()][points.size()];

    for(int i = 0; i < points.size(); i++)
    {
        b[0][i] = points[i];
    }

    for(int j = 1; j < points.size(); j++)
    {
        for(int i = 0; i < points.size() - j; i++)
        {
            b[j][i] = (1-t)*b[j-1][i] + t*b[j-1][i+1];
        }
    }
    return b[points.size()-1][0];
}

std::vector<Vector2f> Curve::GetDeCasteljauPoints(int numPoints)
{
    std::vector<Vector2f> dcPoints;
    for(int i = 0; i <= numPoints; i++)
    {
        dcPoints.push_back(DeCasteljau((float)i/numPoints));
    }
    return dcPoints;
}

Vector2f Curve::DeBoor(int I, int k, float uBar)
{
    Vector2f d[points.size()][points.size()];

    for(int i = 0; i < points.size(); i++)
    {
        d[0][i] = points[i];
    }

    for(int j = 1; j <= k - 1; j++)
    {
        for(int i = I - (k-1); i <= I - j; i++)
        {
            d[j][i] = ((u[i+k] - uBar)/(u[i+k] - u[i+j]))*d[j-1][i] + ((uBar - u[i+j])/(u[i+k] - u[i+j]))*d[j-1][i+1];
        }
    }
    return d[k-1][I-(k-1)];
}

std::vector<Vector2f> Curve::GetDeBoorPoints(int numPoints)
{
    std::vector<Vector2f> dcPoints;
    if(points.size() < k)
        return dcPoints;
    if(points.size() > 2)
    {
        for(float uBar = u[k-1]; uBar <= u[points.size()]; uBar += (u[points.size()] - u[k-1])/numPoints)
        {
            int I;
            for(int i = 0; i < u.size()-1; i++)
            {
                if((u[i] <= uBar) && (u[i+1] > uBar))
                    I = i;
            }
            dcPoints.push_back(DeBoor(I, k, uBar));
        }
    }
    return dcPoints;
}

std::vector<float> Curve::GenerateKnots()
{
    u.clear();
    for(int i = 0; i < points.size()+k; i++)
    {
        u.push_back((float)i);
    }
    return u;
}

std::string Curve::KnotsToString()
{
    std::stringstream ss;

    for(float i : u)
    {
         ss << i << " ";
    }
    return ss.str();
}

std::vector<float> Curve::StringToKnots(std::string knotString)
{
    std::vector<float> knots;
    std::istringstream iss(knotString);
    for(float i; iss >> i;)
        knots.push_back(i);

    u = knots;
    return knots;
}
