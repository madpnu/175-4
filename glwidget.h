#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMouseEvent>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "curve.h"

#include <Eigen/Eigen>
using namespace Eigen;


class GLWidget :
        public QOpenGLWidget,
        protected QOpenGLFunctions
{
    Q_OBJECT
public:
    explicit GLWidget(QWidget *parent = 0);
    ~GLWidget();

    void OpenData(std::string filename);
    void SaveData(std::string filename);
    void NewCurve();
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void teardownGL();

    std::vector<Curve> curves;
    int currentCurve = 0;
    std::string action = "AddPoint";

    Vector2f point1 = {0, 0};
    Vector2f point2 = {0, 0};
    int selectedIndex = 0;
    bool inserting = false;
    bool bezierCurve = true;

private:
     // Private Helpers
    void printContextInformation();
    std::vector<Vector2f> points;
};
