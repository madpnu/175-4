#ifndef CURVE_H
#define CURVE_H

#include <tuple>
#include <Eigen/Eigen>

using namespace Eigen;

class Curve
{
public:
    Curve();
    Curve(int k, bool bezierCurve);
    void AddPoint(Vector2f point);
    void AddPoint(float x, float y);
    void ChangePoint(int index, Vector2f point);
    void InsertPoint(int index, Vector2f point);
    void RemovePoint(int index);
    std::tuple<int, float> ClosestPoint(Vector2f point);
    Vector2f DeCasteljau(float t);
    std::vector<Vector2f> GetDeCasteljauPoints(int numPoints);
    Vector2f DeBoor(int I, int k, float uBar);
    std::vector<Vector2f> GetDeBoorPoints(int numPoints);
    std::vector<float> GenerateKnots();
    std::string KnotsToString();
    std::vector<float> StringToKnots(std::string knotString);

    std::vector<Vector2f> points;
    std::vector<float> u;
    int k;
    bool bezierCurve = true;

};

#endif // CURVE_H
