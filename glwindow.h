#ifndef GLWINDOW_H
#define GLWINDOW_H

#include <QMainWindow>
#include "curve.h"
#include <sstream>

namespace Ui {
class GLWindow;
}

class GLWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit GLWindow(QWidget *parent = 0);
    ~GLWindow();

private slots:
    void on_spinCurveID_valueChanged(int i);

    void on_radioAddPoint_toggled(bool checked);

    void on_radioMovePoint_toggled(bool checked);

    void on_radioRemovePoint_toggled(bool checked);

    void on_radioInsertPoint_toggled(bool checked);

    void on_radioBezier_toggled(bool checked);

    void on_radioBSpline_toggled(bool checked);

    void on_spinOrder_valueChanged(int arg1);

    void on_buttonChangeKnots_clicked();

    void on_buttonNew_clicked();

    void on_buttonRefreshKnots_clicked();

private:
    Ui::GLWindow *ui;
};

#endif // GLWINDOW_H
