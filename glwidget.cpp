#include "glwidget.h"
#include <QDebug>
#include <tuple>
#include <iostream>
#include <fstream>
#include <Eigen/Eigen>

using namespace Eigen;

GLWidget::GLWidget(QWidget *parent)
{
    // Set OpenGL Version information
    // Note: This format must be set before show() is called.
    QSurfaceFormat format;
    format.setRenderableType(QSurfaceFormat::OpenGL);
    format.setProfile(QSurfaceFormat::CompatibilityProfile);
    format.setVersion(2,1);

    this->setFormat(format);
}

GLWidget::~GLWidget()
{
    makeCurrent();
    teardownGL();
}

void GLWidget::OpenData(std::string filename)
{
    curves.clear();
    std::ifstream infile(filename);
    std::string line;
    int countCurves;
    std::getline(infile, line);

    std::istringstream(line) >> countCurves;

    for(int curveIndex = 0; curveIndex < countCurves; curveIndex++)
    {
        do //remove empty lines
        {
            std::getline(infile, line);
        }
        while(line.empty());
        int countPoints;
        std::istringstream(line) >> countPoints;
        Curve newCurve = Curve();

        for(int pointIndex = 0; pointIndex < countPoints; pointIndex++)
        {
            std::getline(infile, line);
            float x, y;
            if(!(std::istringstream(line) >> x >> y))
            {
                break;
                std::cout << "Failed to read point " << line << ": must be formatted like \"0.32 0.0\"" << endl;
                std::cin >> line;
                exit(-1);
            }
            newCurve.AddPoint({x, y});
        }

        std::getline(infile, line);
        std::istringstream(line) >> newCurve.bezierCurve;

        newCurve.u.clear();
        if(!(newCurve.bezierCurve))
        {
            std::getline(infile, line);
            std::istringstream(line) >> newCurve.k;

            int countKnots;
            std::getline(infile, line);
            std::istringstream(line) >> countKnots;

            for(int knotIndex = 0; knotIndex < countKnots; knotIndex++)
            {
                std::getline(infile, line);
                float x;
                if(!(std::istringstream(line) >> x))
                {
                    break;
                    std::cout << "Failed to read point " << line << ": must be formatted like \"0.32\"" << endl;
                    std::cin >> line;
                    exit(-1);
                }
                newCurve.u.push_back(x);
            }
        }
        this->curves.push_back(newCurve);
    }
    infile.close();
}

void GLWidget::SaveData(std::string filename)
{
    std::ofstream fout(filename);

    fout << this->curves.size() << std::endl;

    for(Curve curve : this->curves)
    {
        fout << std::endl;
        fout << curve.points.size() << std::endl;
        for(Vector2f point : curve.points)
        {
            fout << point(0) << " " << point(1) << std::endl;
        }
        fout << curve.bezierCurve << std::endl;
        if(!(curve.bezierCurve))
        {
            fout << curve.k << std::endl;
            fout << curve.u.size() << std::endl;
            for(float knot : curve.u)
            {
                fout << knot << std::endl;
            }
        }
    }

    fout.close();
}

void GLWidget::NewCurve()
{

}

void
GLWidget::initializeGL(){
    // Initialize OpenGL Backend
    initializeOpenGLFunctions();
    printContextInformation();

    // Set global information
    glClearColor(0.0f,0.0f,0.0f,0.0f);

    printContextInformation();
}

void
GLWidget::resizeGL(int width, int height){
    /*set up projection matrix to define the view port*/

    //creates a rendering area across the window
    glViewport(0,0,width,height);

    // uses an orthogonal projection matrix so that
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    //you guys should look up this function
    glOrtho(0,1.0,0.0,1.0,-10,10);

    //clear the modelview matrix
    //the ModelView Matrix can be used in this project, to change the view on the projection
    //but you can also leave it alone and deal with changing the projection to a different view
    //for project 2, do not use the modelview matrix to transform the actual geometry, as you won't
    //be able to save the results
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void
GLWidget::paintGL(){
    //clears the screen
    glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
    //clears the opengl Modelview transformation matrix
    glLoadIdentity();

    glPointSize(3);

    //tells opengl to interperate every two calls to glVertex as a line

    for(Curve curve : curves)
    {
        glBegin(GL_POINTS);
        glColor3f(1.0,0,0);

        for(Vector2f point : curve.points)
        {
            glVertex2f(point(0), point(1));
        }
        glEnd();

        glBegin(GL_LINE_STRIP);
        glColor3f(0.3,0.3,0.0);

        for(Vector2f point : curve.points)
        {
            glVertex2f(point(0), point(1));
        }

        glEnd();

        glBegin(GL_LINE_STRIP);
        glColor3f(0,0,1.0);

        if(curve.bezierCurve)
        {
            for(Vector2f point : curve.GetDeCasteljauPoints(100))
            {
                glVertex2f(point(0), point(1));
            }
        }
        else
        {
            if (curve.k > 1)
            {
            for(Vector2f point : curve.GetDeBoorPoints(100))
            {
                glVertex2f(point(0), point(1));
            }
            }
        }

        glEnd();
    }

}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    float x = (float)event->x()/this->width();
    float y = (float)(this->height() - event->y())/this->height();

    point1 = {x, y};
    point2 = point1;

    if(0 < curves.size())
    {
        if(action == "AddPoint")
        {
            curves[currentCurve].AddPoint(x, y);
        }
        else
        {
            std::tuple<int, float> tuple = curves[currentCurve].ClosestPoint({x, y});
            int minIndex = std::get<0>(tuple);
            float min = std::get<1>(tuple);

            std::cout << minIndex << ", " << min << std::endl;
            if(0.015 > min)
            {
                selectedIndex = minIndex;
            }
            else
            {
                selectedIndex = -1;
            }
            if((0 <= selectedIndex) && (action == "RemovePoint"))
            {
                curves[currentCurve].RemovePoint(selectedIndex);
            }
        }
    }

    paintGL();
    update();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    float x = (float)event->x()/this->width();
    float y = (float)(this->height() - event->y())/this->height();

    if(event->buttons() == Qt::LeftButton)
    {
        if((0 <= selectedIndex) && (action == "InsertPoint"))
        {
            inserting = true;
            point2 = {x, y};
        }
        else if((0 <= selectedIndex) && (action == "MovePoint"))
        {
            point2 = {x, y};
            curves[currentCurve].ChangePoint(selectedIndex, point2);
        }
    }

    paintGL();
    update();
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    float x = (float)event->x()/this->width();
    float y = (float)(this->height() - event->y())/this->height();

    if((0 <= selectedIndex) && (inserting))
    {
        inserting = false;
        point2 = {x, y};
        curves[currentCurve].InsertPoint(selectedIndex, point2);
    }

    paintGL();
    update();
}

void
GLWidget::teardownGL(){
    ;
}

void
GLWidget::printContextInformation(){
    QString glType;
    QString glVersion;
    QString glProfile;

    // Get Version Information
    glType = (context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL";
    glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));

    // Get Profile Information
    #define CASE(c) case QSurfaceFormat::c: glProfile = #c; break
    switch (format().profile())
    {
    CASE(NoProfile);
    CASE(CoreProfile);
    CASE(CompatibilityProfile);
    }
    #undef CASE

    // qPrintable() will print our QString w/o quotes around it.
    qDebug() << qPrintable(glType) << qPrintable(glVersion) << "(" << qPrintable(glProfile) << ")";
}
