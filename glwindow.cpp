#include "glwindow.h"
#include "ui_glwindow.h"
#include <QtGlobal>
#include <Eigen/Eigen>

using namespace Eigen;

GLWindow::GLWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GLWindow)
{
    ui->setupUi(this);

    QObject::connect(ui->buttonLoad, &QPushButton::clicked, [=]()
    {
        ui->openGLWidget->OpenData((ui->lineEditFilename->text()).toUtf8().constData());

        ui->spinOrder->setValue(ui->openGLWidget->curves[ui->spinCurveID->value()].k);

        if(ui->openGLWidget->curves[ui->spinCurveID->value()].bezierCurve)
        {
            ui->radioBezier->setChecked(true);
        }
        else
        {
            ui->radioBSpline->setChecked(true);
        }

        ui->lineKnots->setText(QString::fromStdString(ui->openGLWidget->curves[ui->spinCurveID->value()].KnotsToString()));

        ui->openGLWidget->repaint();
    });
    QObject::connect(ui->buttonSave, &QPushButton::clicked, [=]()
    {
        ui->openGLWidget->SaveData((ui->lineEditFilename->text()).toUtf8().constData());
    });
}

GLWindow::~GLWindow()
{
    delete ui;
}

void GLWindow::on_buttonNew_clicked()
{
    ui->openGLWidget->curves.push_back(Curve(ui->spinOrder->value(), ui->openGLWidget->bezierCurve));
    ui->spinCurveID->setMaximum(this->ui->openGLWidget->curves.size() - 1 * (this->ui->openGLWidget->curves.size() > 0));
    ui->spinCurveID->setValue(this->ui->openGLWidget->curves.size() - 1 * (this->ui->openGLWidget->curves.size() > 0));
}

void GLWindow::on_spinCurveID_valueChanged(int i)
{
    ui->openGLWidget->currentCurve = i;

    ui->spinOrder->setValue(ui->openGLWidget->curves[i].k);

    if(ui->openGLWidget->curves[i].bezierCurve)
    {
        ui->radioBezier->setChecked(true);
    }
    else
    {
        ui->radioBSpline->setChecked(true);
    }

    ui->lineKnots->setText(QString::fromStdString(ui->openGLWidget->curves[i].KnotsToString()));

    ui->openGLWidget->repaint();
}

void GLWindow::on_spinOrder_valueChanged(int i)
{
    if(ui->openGLWidget->curves.size() > 0)
    {
        ui->openGLWidget->curves[ui->spinCurveID->value()].k = i;
    }
    ui->openGLWidget->repaint();
}

void GLWindow::on_radioAddPoint_toggled(bool checked)
{
    if(checked)
    {
         ui->openGLWidget->action = "AddPoint";
    }
}

void GLWindow::on_radioInsertPoint_toggled(bool checked)
{
    if(checked)
    {
         ui->openGLWidget->action = "InsertPoint";
    }
}

void GLWindow::on_radioMovePoint_toggled(bool checked)
{
    if(checked)
    {
         ui->openGLWidget->action = "MovePoint";
    }
}

void GLWindow::on_radioRemovePoint_toggled(bool checked)
{
    if(checked)
    {
         ui->openGLWidget->action = "RemovePoint";
    }
}

void GLWindow::on_radioBezier_toggled(bool checked)
{
    if(checked)
    {
        ui->openGLWidget->bezierCurve = true;
        ui->openGLWidget->curves[ui->spinCurveID->value()].bezierCurve = true;
    }
    ui->openGLWidget->repaint();
}

void GLWindow::on_radioBSpline_toggled(bool checked)
{
    if(checked)
    {
        ui->openGLWidget->bezierCurve = false;
        if(ui->openGLWidget->curves.size() > 0)
        {
            ui->openGLWidget->curves[ui->spinCurveID->value()].bezierCurve = false;
            if(ui->openGLWidget->curves[ui->spinCurveID->value()].u.size() == 0)
            {
                ui->openGLWidget->curves[ui->spinCurveID->value()].GenerateKnots();
            }
        }
    }
    ui->openGLWidget->repaint();
}

void GLWindow::on_buttonRefreshKnots_clicked()
{
     ui->lineKnots->setText(QString::fromStdString(ui->openGLWidget->curves[ui->spinCurveID->value()].KnotsToString()));
}

void GLWindow::on_buttonChangeKnots_clicked()
{
    std::vector<float> knots = ui->openGLWidget->curves[ui->spinCurveID->value()].StringToKnots(ui->lineKnots->text().toStdString());

    ui->openGLWidget->repaint();
}
